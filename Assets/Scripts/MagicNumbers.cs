﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MagicNumbers : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI guessText;

    int min = 1;
    int max = 1000;
    int guess;
    string myName = "Denis";
    bool check = false;



    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }

    void StartGame()
    {
        guess = CalculateGuess();
        UpdateResult();
    }

    int CalculateGuess()
    {
        
        //int newGuess = (min + max) / 2;
        int newGuess = Random.Range(min, max);
        return newGuess;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SetUp()
    {
        min = guess + 1;
        UpdateResult();
    }

    public void SetDown()
    {
        max = guess - 1;
        UpdateResult();
    }

    void UpdateResult()
    {
        guess = CalculateGuess();
        guessText.text = guess.ToString();
        Debug.Log("Is it: " + guess + "?");
    }
}
